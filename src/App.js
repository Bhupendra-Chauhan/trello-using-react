import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import Boards from "./components/Boards";
import Lists from "./components/Lists";
import "./App.css";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Header />
          <Switch>
            <Route exact path="/" component={Boards} />
            <Route path="/board/:name/:id" component={Lists} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
