import React, { Component } from "react";
import CheckListItems from './CheckListItems';


export class CheckList extends Component {
  render() {
    return (
      <div className="ChecklistContainer">
        <div className="CheckListHeader">
          <h2>{this.props.name}</h2>
          <button
            onClick={() => this.props.DeleteCheckList(this.props.id)}
            className="btn btn-outline-danger"
          >
            Delete
          </button>
        </div>
        <div>
            <div style={{ margin: "10px" }}>
            <CheckListItems id={this.props.id} />
            </div>
        </div>
      </div>
    );
  }
}

export default CheckList;
