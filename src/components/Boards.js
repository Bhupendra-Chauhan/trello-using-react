import React, { Component } from "react";
import Board from "./Board";
import CreateBoard from "./CreateBoard";
import axios from "axios";
import { Link } from "react-router-dom";

class Boards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Boards: [],
    };
  }

  DeleteBoard = (id) => {
    axios
      .delete(
        `https://api.trello.com/1/boards/${id}?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`
      )
      .then((res) => {
        //console.log(res);
        if (res.status === 200) {
          this.setState((prev) => {
            const Filtered = prev.Boards.filter((e) => e.id !== id);
            // console.log(Filtered);
            return {
              Boards: Filtered,
            };
          });
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  createNewBoard = (name) => {
    axios
      .post(
        `https://api.trello.com/1/boards/?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&name=${name}`
      )
      .then((res) => {
        //console.log(res);
        this.setState((prev) => {
          const newBoardsState = [...prev.Boards];
          newBoardsState.push(res.data);
          //console.log(newBoardsState)
          return {
            Boards: newBoardsState,
          };
        });
      })
      .then(() => {
        this.componentDidMount();
      })
      .catch((err) => {
        console.error(err);
      });
  };

  componentDidMount() {
    axios
      .get(
        "https://api.trello.com/1/members/me/boards?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89"
      )
      .then((res) => {
        //console.log(res.data);
        this.setState({ Boards: res.data });
      });
  }
  render() {
    //console.log(this.state.Boards);
    return (
      <div style={BoardsContainer}>
        {this.state.Boards.map((e) => (
          <Link
            to={`/board/${e.name}/${e.id}`}
            key={e.id}
            style={{ textDecoration: "none" }}
            onClick={(event) => {
              if (event.target.tagName === "BUTTON") {
                event.preventDefault();
                this.DeleteBoard(e.id);
              }
            }}
          >
            <Board
              key={e.id}
              id={e.id}
              name={e.name}
              // DeleteBoard={this.DeleteBoard}
              bg={
                e.prefs.backgroundImageScaled
                  ? e.prefs.backgroundImageScaled[2].url
                  : null
              }
              backgroundColor={
                e.prefs.backgroundColor ? e.prefs.backgroundColor : null
              }
            />
          </Link>
        ))}
        <CreateBoard createNewBoard={this.createNewBoard} />
      </div>
    );
  }
}

const BoardsContainer = {
  display: "flex",
  flexWrap: "wrap",
  padding: "10px",
  margin: "10px",
};

export default Boards;
