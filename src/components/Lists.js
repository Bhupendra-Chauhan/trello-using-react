import React, { Component } from "react";
import axios from "axios";
import List from "./List";

class Lists extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Lists: [],
      open: true,
      newListName: "",
    };

    this.AddList = this.AddList.bind(this);
  }

  componentDidMount() {
    axios
      .get(
        `https://api.trello.com/1/boards/${this.props.match.params.id}/lists?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`
      )
      .then((res) => {
        //console.log(res.data);
        this.setState({ Lists: res.data });
      });
  }

  AddList = () => {
    const ListName = this.state.newListName;
    if(ListName!=='')
    axios
      .post(
        `https://api.trello.com/1/lists?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&name=${ListName}&idBoard=${this.props.match.params.id}`
      )
      .then((res) => {
        this.setState((prev) => {
          const newListsState = [...prev.Lists];
          newListsState.push(res.data);

          return {
            Lists: newListsState,
          };
        });
      })
      .then(() => {
        this.componentDidMount();
      })
      .catch((err) => {
        console.error(err);
      });
    this.setState({ open: true, newListName: "" });
  };

  inputHandler = (e) => {
    this.setState({ newListName: e.target.value }, () =>
      console.log(this.state.newListName)
    );
  };

  DeleteList = (id) => {
    axios
      .put(
        `https://api.trello.com/1/lists/${id}/closed?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&value=true`
      )
      .then((res) => {
        if (res.status === 200) {
          this.setState((prev) => {
            const Filtered = prev.Lists.filter((e) => e.id !== id);
            // console.log(Filtered);
            return {
              Lists: Filtered,
            };
          });
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  render() {
    //console.log(this.state.Lists);
    return (
      <div>
        <span className="BoardName">{this.props.match.params.name}</span>
        <div style={ListsContainer}>
          {this.state.Lists.map((e) => {
            return (
              <List
                key={e.id}
                name={e.name}
                id={e.id}
                DeleteList={this.DeleteList}
              />
            );
          })}

          {this.state.open ? (
            <div>
              <span
                className="AddListBtn"
                onClick={() => this.setState({ open: false })}
              >
                + Add another list
              </span>
            </div>
          ) : (
            <div className="AddListBox">
              <textarea
                style={{ width: "288px" }}
                value={this.state.newListName}
                placeholder="Enter list title..."
                onChange={this.inputHandler}
              />
              <div style={{ display: "flex ", padding: "8px" }}>
                <button onClick={this.AddList} className="btn btn-success">
                  Add List
                </button>
                <h1
                  style={{ marginLeft: "3rem" }}
                  onClick={() => this.setState({ open: true })}
                >
                  x
                </h1>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const ListsContainer = {
  display: "flex",
  overflow: "auto",
  height: "89vh",
  padding: "20px",
};

export default Lists;
