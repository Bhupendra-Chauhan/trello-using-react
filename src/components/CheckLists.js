import axios from "axios";
import React, { Component } from "react";
import CheckList from "./CheckList";

class CheckLists extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Checklists: [],
      open: true,
      newCheckListName: "",
    };
  }

  componentDidMount() {
    const id = this.props.id;
    axios
      .get(
        `https://api.trello.com/1/cards/${id}/checklists?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`
      )
      .then((res) => {
        //console.log(res.data);
        this.setState({ Checklists: res.data });
      });
  }

  AddCheckList = () => {
    const id = this.props.id;
    const CheckListName = this.state.newCheckListName;
    if (CheckListName !== "")
      axios
        .post(
          `https://api.trello.com/1/cards/${id}/checklists?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&name=${CheckListName}`
        )
        .then((res) => {
          this.setState((prev) => {
            const newCheckListsState = [...prev.Checklists];
            newCheckListsState.push(res.data);

            return {
              Checklists: newCheckListsState,
            };
          });
        })
        .then(() => {
          this.setState({ newCheckListName: "" });
        })
        .catch((err) => {
          console.error(err);
        });
  };

  inputHandler = (e) => {
    this.setState({ newCheckListName: e.target.value }, () =>
      console.log(this.state.newCheckListName)
    );
  };

  DeleteCheckList = (id) => {
    axios
      .delete(
        `https://api.trello.com/1/checklists/${id}?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`
      )
      .then((res) => {
        if (res.status === 200) {
          this.setState((prev) => {
            const Filtered = prev.Checklists.filter((e) => e.id !== id);
            // console.log(Filtered);
            return {
              Checklists: Filtered,
            };
          });
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  render() {
    return (
      <div className="containerBody">
        <div>
          {this.state.Checklists.map((e) => {
            return <CheckList key={e.id} id={e.id} name={e.name} DeleteCheckList={this.DeleteCheckList }/>;
          })}
        </div>
        <div>
          {this.state.open ? (
            <div>
              <span
                className="AddCheckListBtn"
                onClick={() => this.setState({ open: false })}
              >
                + Checklist
              </span>
            </div>
          ) : (
            <div className="AddListBox">
              <textarea
                style={{ width: "288px" }}
                value={this.state.newCheckListName}
                placeholder="Enter Checklist title..."
                onChange={this.inputHandler}
              />
              <div style={{ display: "flex ", padding: "8px" }}>
                <button onClick={this.AddCheckList} className="btn btn-success">
                  Add CheckList
                </button>
                <h1
                  style={{ marginLeft: "3rem" }}
                  onClick={() => this.setState({ open: true })}
                >
                  x
                </h1>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default CheckLists;
