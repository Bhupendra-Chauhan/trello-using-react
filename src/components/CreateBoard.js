import React, { Component } from "react";
import { Modal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import ModalLayout from "./ModalLayout";

class CreateBoard extends Component {
  state = {
    openModal: false,
  };

  onClickButton = (e) => {
    e.preventDefault();
    this.setState({ openModal: true });
  };

  onCloseModal = () => {
    this.setState({ openModal: false });
  };

  render() {
    //console.log(this.state.openModal);
    return (
      <React.Fragment>
        <div style={CreateBoardContainer} onClick={this.onClickButton}>
          <h2>+ Create a new Board</h2>
        </div>
        <Modal open={this.state.openModal} onClose={this.onCloseModal}>
          <ModalLayout
            AddBoard={this.props.createNewBoard}
            onCloseModal={this.onCloseModal}
          />
        </Modal>
      </React.Fragment>
    );
  }
}

const CreateBoardContainer = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "200px",
  width: "400px",
  margin: "10px",
  borderRadius: "5px",
  backgroundColor: "whitesmoke",
  cursor: "pointer",
};

export default CreateBoard;
