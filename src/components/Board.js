import React, { Component } from "react";

class Board extends Component {
  render() {
    const BoardContainer = {
      height: "200px",
      width: "400px",
      borderRadius: "5px",
      cursor: "pointer",
      margin: "10px",
      backgroundImage: `url(${this.props.bg})`,
      background: "no-repeat cover",
      backgroundSize: "400px 200px",
      backgroundColor: this.props.backgroundColor,
      WebkitBoxShadow: "0px 0px 10px 0px #f5f5f5",
      boxShadow: "0px 0px 10px 0px #f5f5f5",
    };

    return (
      <div style={BoardContainer} id={this.props.id}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            padding: "10px",
          }}
        >
          <h2
            style={{
              color: "#fff",
              backgroundColor: "rgb(0,0,0,0.5)",
              borderRadius: "5px",
            }}
          >
            {this.props.name}
          </h2>
          <button className="btn btn-outline-danger">Delete</button>
        </div>
      </div>
    );
  }
}

export default Board;
