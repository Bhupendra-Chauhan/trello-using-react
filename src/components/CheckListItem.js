import React, { Component } from "react";

export class CheckListItem extends Component {
  render() {
      console.log(this.props.name);
    return (
      
        <div className="CheckListHeader">
            {console.log(this.props.name)}
          <span>{this.props.name}</span>
          <button
            onClick={() => this.props.DeleteCheckListItem(this.props.id)}
            className="btn btn-outline-danger"
          >
            x
          </button>
        </div>
    );
  }
}

export default CheckListItem;
