import React, { Component } from "react";
import PopUp from "./PopUp";

class Card extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             open:false,
             cardId:'',
             cardName:''
        }
    }
    
    handler = () =>{
        this.setState({open:false})
    }


  render() {
   
    return (
        <>
      <div className="CardContainer">
        <span style={{width:'100%',height:'100%'}} onClick={()=>this.setState({open:true, cardId:this.props.id, cardName:this.props.name})} >{this.props.name}</span>
        <button className="btn btn-outline-danger" onClick={()=>{this.props.DeleteCard(this.props.id)}}>X</button>
      </div>
      <PopUp open={this.state.open} id={this.state.cardId} name={this.state.cardName} handler={this.handler}/>
    </>
    );
  }
}

export default Card;
