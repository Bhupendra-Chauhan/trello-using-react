import React, { Component } from 'react'

class ModalLayout extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            newBoardName: ''
        }
    }
    inputHandler = e =>{
        this.setState({newBoardName: e.target.value}, ()=> console.log(this.state.newBoardName));
    }
    render() {
        return (
            <div style={ModalContainer}>
                <input type="text" placeholder="Enter the name" value={this.state.newBoardName} onChange={this.inputHandler}/>
                <button onClick={()=>{this.props.AddBoard(this.state.newBoardName); this.props.onCloseModal(); }} className="btn btn-success">Add new Element</button>
            </div>
        )
    }
}
const ModalContainer={
    display:'flex',
    flexDirection:'column',
    padding:'20px'
}
export default ModalLayout
