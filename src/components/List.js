// import axios from "axios";
import React, { Component } from "react";
import Cards from "./Cards";

class List extends Component {
  render() {
    return (
      <div className="ListContainer">
        <div style={ListHeader}>
          <h2>{this.props.name}</h2>
          <button
            onClick={() => this.props.DeleteList(this.props.id)}
            className="btn btn-outline-danger"
          >
            Delete
          </button>
        </div>
        <div>
          <div style={{ margin: "10px" }}>
            <Cards id={this.props.id} />
          </div>
        </div>
      </div>
    );
  }
}

const ListHeader = {
  display: "flex",
  justifyContent: "space-between",
  minWidth: "400px",
  borderRadius: "5px",
  margin: "10px",
};

export default List;
