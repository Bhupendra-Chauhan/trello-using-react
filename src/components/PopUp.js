import React, { Component } from "react";
import CheckLists from "./CheckLists";


export class PopUp extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            open:true,
            name:this.props.name
        }
    }
    
  render() {
    //console.log(this.props);
    if(this.props.open)
    return (
        
      <div className="containerWrapper">
        <div className="container">
          <div className="containerHeader">
            <h2 className="containerName">{this.props.name}</h2>
            <button className="btn btn-secondary" onClick={this.props.handler}>
              x
            </button>
          </div>
            <div className="checklistContainer">
                
                <CheckLists id={this.props.id} name={this.props.name}/>
            
            </div>
        </div>
      </div>
    );
    return null
  }
}

export default PopUp;