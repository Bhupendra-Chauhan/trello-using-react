import React, { Component } from "react";
import CheckListItem from "./CheckListItem";
import axios from 'axios';

class CheckListItems extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Checklistitems: [],
      open: true,
      newCheckListItemsName: ''
    };
  }

  componentDidMount() {
    const id = this.props.id;
    axios
      .get(
        `https://api.trello.com/1/checklists/${id}/checkItems?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`
      )
      .then((res) => {
        this.setState({ Checklistitems: res.data });
      });
  }

  inputHandler = (e) => {
    this.setState({ newCheckListItemsName: e.target.value }, () =>
      console.log(this.state.newCheckListItemsName)
    );
    
};

  AddCheckListItem = () =>{
    const id = this.props.id;
    const CheckListItemName = this.state.newCheckListItemsName;
    if (CheckListItemName !== "")
      axios
        .post(
          `https://api.trello.com/1/checklists/${id}/checkItems?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&name=${CheckListItemName}`
        )
        .then((res) => {
          this.setState((prev) => {
            const newCheckListItemsState = [...prev.Checklistitems];
            newCheckListItemsState.push(res.data);

            return {
              Checklistitems: newCheckListItemsState,
            };
          });
        })
        .then(() => {
          this.setState({ newCheckListItemsName: ''});
        })
        .catch((err) => {
          console.error(err);
        });
  }


  DeleteCheckListItem = (id) =>{
    const p_id = this.props.id;
    axios
    .delete(
        `https://api.trello.com/1/checklists/${p_id}/checkItems/${id}?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`
    )
    .then((res) => {
      if (res.status === 200) {
        this.setState((prev) => {
          const Filtered = prev.Checklistitems.filter((e) => e.id !== id);
          //console.log(Filtered);
          return {
            Checklistitems: Filtered,
          };
        });
      }
    })
    .catch((err) => {
      console.error(err);
    });
  }

  render() {
    //   console.log(this.state.Checklistitems);
    return (
      <div>
        <div>
          {this.state.Checklistitems && this.state.Checklistitems.map((e) => {
            return <CheckListItem key={e.id} id={e.id} name={e.name} DeleteCheckListItem={this.DeleteCheckListItem}/>;
          })}
        </div>
        <div>
          {this.state.open ? (
            <div
              className="AddItemBtn"
              onClick={() => this.setState({ open: false })}
            >
              Add an Item
            </div>
          ) : (
            <div className="AddListBox">
              <textarea
                style={{ width: "288px" }}
                value={this.state.newCheckListItemsName}
                placeholder="Enter item title..."
                onChange={this.inputHandler}
              />
              <div style={{ display: "flex ", padding: "8px" }}>
                <button style={{width:'120px', height:'40px', marginTop:'8px'}}onClick={this.AddCheckListItem} className="btn btn-success">
                  Add
                </button>
                <h1
                  style={{ marginLeft: "3rem" }}
                  onClick={() => this.setState({ open: true })}
                >
                  x
                </h1>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default CheckListItems;
