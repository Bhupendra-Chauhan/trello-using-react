import axios from "axios";
import React, { Component } from "react";
import Card from "./Card";

class Cards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Cards: [],
      open: true,
      newCardName: "",
    };
  }

  componentDidMount() {
    axios
      .get(
        `https://api.trello.com/1/lists/${this.props.id}/cards?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`
      )
      .then((res) => {
        //console.log(res.data);
        this.setState({ Cards: res.data });
      });
  }

  AddNewCard = () => {
    const CardName = this.state.newCardName;
    if(CardName!=='')
    axios
      .post(
        `https://api.trello.com/1/cards?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&idList=${this.props.id}&name=${CardName}`
      )
      .then((res) => {
        //   console.log(res)
        this.setState((prev) => {
          const newCardsState = [...prev.Cards];
          newCardsState.push(res.data);

          return {
            Cards: newCardsState,
          };
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .then(() => {
        this.setState({ newCardName: "" });
      });
  };

  inputHandler = (e) => {
    this.setState({ newCardName: e.target.value }, () =>
      console.log(this.state.newCardName)
    );
  };

  DeleteCard = (id) =>{
      axios.delete(`https://api.trello.com/1/cards/${id}?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`)
      .then((res)=>{
        if (res.status === 200) {
            this.setState((prev) => {
              const Filtered = prev.Cards.filter((e) => e.id !== id);
              // console.log(Filtered);
              return {
                Cards: Filtered,
              };
            });
          }
      })
      .catch((err)=>{
          console.error(err);
      })
  }





  render() {
    return (
      <div>
        {this.state.Cards.map((e) => {
          return <Card key={e.id} name={e.name} id={e.id} DeleteCard={this.DeleteCard} />;
        })}
        {this.state.open ? (
          <div
            className="AddCardBtn"
            onClick={() => this.setState({ open: false })}
          >
            + Add a card
          </div>
        ) : (
          <div className="AddCardContainer">
            {/* <input type="text"  placeholder="Enter a title for this card..."/> */}
            <textarea
              placeholder="Enter a title for this card..."
              value={this.state.newCardName}
              onChange={this.inputHandler}
            />
            <div style={{ display: "flex", paddingTop: "10px" }}>
              <button
                className="btn btn-success"
                style={{ marginRight: "3rem" }}
                onClick={this.AddNewCard}
              >
                Add Card
              </button>
              <h1 onClick={() => this.setState({ open: true })}>x</h1>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Cards;
